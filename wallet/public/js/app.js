var app = angular.module("WalletTest", ["ngSanitize"]);

app.controller('WalletTestCtrl', function ($scope, $http) {
  $scope.showBtn = false;

  //fonction d'initialisation d'affichage des wallets
  $scope.initFirst = function () {
    $http({
      method: "GET",
      url: "/api/rechercher?mode=all"
    }).then(function mySucces(response) {
      $scope.wallet = response.data;
      $scope.filtreWallet = response.data.length;
      $scope.totalWallet = response.data.length;
      $scope.search = '';
      $scope.searchIdentifiant = '';
      $scope.newClef = '';
      $scope.newName = '';
      $scope.newLogin = '';
      $scope.newPwd = '';
      $scope.newComment = '';
    }, function myError(response) {
      $scope.wallet = response.statusText;
    });
  };

  // Ajouter un wallet
  $scope.addWallet = function () {
    $http({
      method: "POST",
      url: "/api/ajouter?id=" + $scope.newClef + "&name=" + $scope.newName + "&login=" + $scope.newLogin + "&pwd=" + $scope.newPwd + "&comment=" + $scope.newComment,
    }).then(function mySucces(response) {
      $scope.myresponse = response.data;
      $scope.alerting = true;
      $scope.typeMessage = 'alert-success';
      console.log($scope.typeMessage);
      console.log($scope.myresponse);
      console.log("initfirst");
    }, function myError(response) {
      $scope.mystatus = response.statusText;
      $scope.myresponse = response.data;
      console.log($scope.myresponse);
      $scope.alerting = true;
      $scope.typeMessage = 'alert-warning';
      console.log($scope.typeMessage);
    }).then(function recharger() {
      $scope.initFirst();
    });
  };

  // Modifier un wallet
  $scope.UpdateWallet = function () {
    $http({
      method: "POST",
      url: "/api/modifier?id=" + $scope.newClef + "&name=" + $scope.newName + "&login=" + $scope.newLogin + "&pwd=" + $scope.newPwd + "&comment=" + $scope.newComment,
    }).then(function mySucces(response) {
      $scope.showBtn = false;
      $scope.myresponse = response.data;
      $scope.alerting = true;
      $scope.typeMessage = 'alert-success';
      console.log($scope.typeMessage);
      console.log($scope.myresponse);
    }, function myError(response) {
      $scope.mystatus = response.statusText;
      $scope.myresponse = response.data;
      console.log($scope.myresponse);
      $scope.alerting = true;
      $scope.typeMessage = 'alert-warning';
      console.log($scope.typeMessage);
    }).then(function recharger() {
      $scope.initFirst();
    });
  };

  // Supprimer un wallet
  $scope.removeWallet = function (wallet) {
    $http({
      method: "DELETE",
      url: "/api/supprimer?id=" + wallet.id,
      withCredentials: true,
      headers: {
        'Authorization': 'Basic ' + btoa($scope.username + ":" + $scope.password)
      }
    }).then(function mySucces(response) {
      $scope.myresponse = response.data;
      $scope.alerting = true;
      $scope.typeMessage = 'alert-success';
      console.log($scope.myresponse);
      //rechargement des données
      $scope.initFirst();

    }, function myError(response) {
      $scope.myresponse = response.statusText;
      $scope.alerting = true;
      $scope.typeMessage = 'alert-warning';
      console.log($scope.typeMessage);

    }).then(function recharger() {
      $scope.initFirst();

    }); 
  };

  // Editer un wallet
  $scope.editWallet = function (wallet) {
    $scope.showBtn = true;
    $scope.newClef = wallet.id;
    $scope.newName = wallet.name;
    $scope.newLogin = wallet.login;
    $scope.newPwd = wallet.pwd;
    $scope.newComment = wallet.comment;
  };

  // Rechercher un wallet par son Url
  $scope.searchWallet = function (search) {
    $http({
      method: "GET",
      url: "/api/rechercher?name=" + search
    }).then(function mySucces(response) {
      $scope.wallet = response.data;
      $scope.filtreWallet = response.data.length;
    }, function myError(response) {
      $scope.wallet = response.statusText;
    });
  };

  // Rechercher un wallet par son identifiant
  $scope.searchLogin = function (searchIdentifiant) {
    $http({
      method: "GET",
      url: "/api/rechercher?login=" + searchIdentifiant
    }).then(function mySucces(response) {
      $scope.wallet = response.data;
      $scope.filtreWallet = response.data.length;
    }, function myError(response) {
      $scope.wallet = response.statusText;
    });
  };



  // Suppression de la base de wallet
  $scope.clearWallet = function () {
    $http({
      method: "DELETE",
      url: "/api/deleteall"
    }).then(function mySucces(response) {
      $scope.wallet = response.data;
      $scope.filtreWallet = response.data.length;
    }, function myError(response) {
      $scope.wallet = response.statusText;
    });
    $scope.initFirst();
  };

  // Peuplement de la base de wallet
  $scope.initWallet = function () {
    $http({
      method: "DELETE",
      url: "/api/datatest"
    }).then(function mySucces(response) {
      $scope.wallet = response.data;
      $scope.filtreWallet = response.data.length;
    }, function myError(response) {
      $scope.wallet = response.statusText;
    });
    $scope.initFirst();
  };

  $scope.closeAlert = function () {
    $scope.alerting = false;
  };

  $scope.videWallet = function () {
    $scope.newClef = '';
    $scope.newName = '';
    $scope.newLogin = '';
    $scope.newPwd = '';
    $scope.newComment = '';
  };
  //fin
});
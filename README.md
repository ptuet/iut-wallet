# IUT-WALLET

Les TP concernant le test nécessitent l'application mise sous test appelée Wallet disponible dans les documents ci dessous. Pour faciliter les travaux en séance, vous devrez vous munir de votre ordinateur portable en disposant des droits d'accès administrateur sur celui-ci. L'exécution de l'application nécessite de disposer de nodeJs. Les explications ci dessous sont données à titre indicatif pour une installation sous Windows. L'installation est également possible sous Linux (ubuntu), non décrit dans cet article. 

## Préparation
1. Si NodeJs n'est pas installé sur votre poste, veuillez passer par l'installation: https://nodejs.org/en/. Installer la version 6.9.4 (LTS) par défaut

2. Installer Git Bash depuis https://git-for-windows.github.io/ en mode par défaut

3. Redémarrer votre poste après installation

4. Après redémarrage, lancer git bash et vérifier la version de NodeJs

5. Créer un répertoire c:/test

6. Se positionner dans le répertoire test et récupérer le code de l'application à mettre sous test:

`git clone https://gitlab.com/fabrice.marques/iut-wallet.git`

## Démarrage

Lancer un nouveau terminal Git bash et se positionner dans le répertoire wallet puis exécuter:

``` 
cd /c/test/iut-wallet/wallet/
npm install
npm start 
```

Vous devez obtenir le message : Démarrage de l'application sur le port 8097.

Tester le démarrage de l'application en ouvrant un navigateur (firefox ou chrome): http://localhost:8097